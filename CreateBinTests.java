package services;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

import java.util.Arrays;

import org.apache.http.HttpStatus;
import org.junit.Test;


public class CreateBinTests
{
	private static String createBin = "https://api.jsonbin.io/v3";

	@Test
	public void createBin(){
		String key = "X-Master-Key":"<YOUR_API_KEY>";

		given().
		param("key", "X-Master-Key").
		when().
		get(createBin)
		.then().
		statusCode(HttpStatus.SC_OK).
		body(	"Content-Type", equalTo(1),
				"X-Master-Key", equalTo("X-Master-Key"),
				"X-Bin-Private", containsInAnyOrder("True"),
				"X-Bin-Name", containsInAnyOrder((Object)Arrays.asList("ABC")),
				"X-Collection-Id", containsInAnyOrder("123");
	}
}

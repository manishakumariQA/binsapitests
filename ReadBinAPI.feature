Feature: Bins API - Read

Background: Init Step
            Given we prepare a new api session called readBinAPITest
            Given we store {"url":"https://api.jsonbin.io/v3"} in the context


  Scenario: User reads a Bin
    Given we prepare a new api session called readBin
    And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
    And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to readBin
    Given we store {"payload": {"licensePlate": "<licensePlate>"} } in the context
    When we perform a GET against context(url)/b/<BIN_ID> using payload of context(payload) using readBin
    Then assert that 200 will be equal to object(readBin.statuscode) as expected
    And response includes the following
    	| record 	 		| 1 		  |
    	| sample			| Hello World |
    	| id			    | BIN_ID	  |
    	| private			| true		  |

    Scenario: User reads a specific Bin Version
        Given we prepare a new api session called readBinVersion
        And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
        And we store {"payload": {"BIN_Version": "<BIN_Version>"} } in the context
        And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to readBinVersion
        When we perform a GET against context(url)/b/<BIN_ID>/<BIN_VERSION> using payload of context(payload) using readBinVersion
        Then assert that 200 will be equal to object(readBinVersion.statuscode) as expected
        And response includes the following
        	| record 	 		| 1 		  |
        	| sample			| Hello World |
        	| id			    | BIN_ID	  |
        	| private			| true		  |
        	| version           | Bin_Version |


    Scenario: User reads the latest record of the Bin
        Given we prepare a new api session called readLatestRecordOfBin
        And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
        And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to readLatestRecordOfBin
        When we perform a GET against context(url)/b/<BIN_ID>/latest  using payload of context(payload) using readLatestRecordOfBin
        Then assert that 200 will be equal to object(readLatestRecordOfBin.statuscode) as expected
        And assert that the latest updated record is returned


      Scenario: User reads a Bin <param>
       Given we prepare a new api session called readBinNegative
       And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
       And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to readBinNegative
       When we perform a GET against context(url)/b/<BIN_ID>  using payload of context(payload) using readBinNegative
       Then assert that 400 Bad Request will be equal to object(readBinNegative.statuscode) as expected
       And assert that object(readBinNegative.message) will be equal to <message> as expected
       Examples:
           | param                                             | BIN_ID                               | message                  |
           | Invalid Bin Id provided                           | Invalid BIN_ID                       | Invalid Bin Id provided  |
           | Bin version is invalid                            | Invalid BIN_VERSION                  | Bin version is invalid   |


      Scenario: User reads a Bin <param>
        Given we prepare a new api session called readBinNegative
        And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
        And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to readBinNegative
        When we perform a GET against context(url)/b/<BIN_ID>  using payload of context(payload) using readBinNegative
        Then assert that 404 Not Found will be equal to object(readBinNegative.statuscode) as expected
        And assert that object(readBinNegative.message) will be equal to <message> as expected
        Examples:
            | param                                             | BIN_ID                             | message
            | Bin not found or it doesn't belong to your account| random value                       | Bin not found or it doesn't belong to your account |
            | Bin version not found                             | random value of BIN_VERSION        | Bin version not found                              |

       Scenario: User reads a Bin <param>
        Given we prepare a new api session called readBinNegative
        And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
        When we perform a POST against context(url)/b/<BIN_ID>  using payload of context(payload) using readBinNegative
        Then assert that 401 unauthorized will be equal to object(readBinNegative.statuscode) as expected
        And assert that object(readBinNegative.message) will be equal to <message> as expected
        Examples:
            | param                                             | X-Master-Key                       | message
            | Empty X-Master-Key                                |                                    | You need to pass X-Master-Key in the header to read a private bin |
            | Invalid X-Master-Key                              | random invalid value               | X-Master-Key is invalid or the bin doesn't belong to your account |
            | Non associated Bin                                | random value                       | Bin not associated to any user account                            |



       Scenario: User reads a Bin many times
        Given we prepare a new api session called readBinNegative
        And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
        When we perform a POST against context(url)/b/<BIN_ID>  using payload of context(payload) using readBinNegative
        Then assert that 403 Forbidden will be equal to object(readBinNegative.statuscode) as expected
        And assert that object(readBinNegative.message) will be equal to Requests exhausted. Buy additional requests at https://jsonbin.io/pricing as expected

        Scenario: User reads a Bin many times
         Given we prepare a new api session called readBinNegative
         And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
         When we perform a POST against context(url)/b/<BIN_ID>  using payload of context(payload) using readBinNegative
         Then assert that 403 Not Found will be equal to object(readBinNegative.statuscode) as expected
         And assert that object(readBinNegative.message) will be equal to Bins not associated to any user are now blocked. Contact the admin at https://jsonbin.io/contact for further info as expected









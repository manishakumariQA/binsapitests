package services;

import org.apache.http.HttpStatus;
import org.junit.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;


public class UpdateBinTests
{
	private static String updateBin = "https://api.jsonbin.io/v3";

	@Test
	public void updateBin(){
		String key = "X-Master-Key":"<YOUR_API_KEY>";

		given().
		param("key", "X-Master-Key").
		when().
		get(updateBin)
		.then().
		statusCode(HttpStatus.SC_OK).
		body(	"Content-Type", equalTo(1),
				"X-Master-Key", equalTo("X-Master-Key"),
				"X-Bin-Versioning", containsInAnyOrder("True");
	}
}

Feature: Bins API - Delete

Background: Init Step
            Given we prepare a new api session called deleteBinAPITest
            Given we store {"url":"https://api.jsonbin.io/v3"} in the context


  Scenario: User deletes a Private Bin with version
    Given we prepare a new api session called deleteBin
    And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
    And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to deleteBin
    When we perform a DELETE against context(url)/b/<BIN_ID> using payload of context(payload) using deleteBin
    Then assert that 200 will be equal to object(deleteBin.statuscode) as expected
    And response includes the following
    	| id	 		    | Bin_ID		              |
    	| versionsDeleted   | 1                           |
    	| message			| Bin deleted successfully	  |


  Scenario: User deletes a Public Bin with version
      Given we prepare a new api session called deleteBin
      And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
      And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to deleteBin
      When we perform a DELETE against context(url)/b/<BIN_ID> using payload of context(payload) using deleteBin
      Then assert that 200 will be equal to object(deleteBin.statuscode) as expected
      And response includes the following
        | id	 		    | Bin_ID		              |
        | versionsDeleted   | 1                           |
        | message			| Bin deleted successfully	  |



      Scenario: User deletes a Bin <param>
       Given we prepare a new api session called deleteBinNegative
       And we store {"payload": {"BIN_ID": "<Invalid BIN_ID>"} } in the context
       And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to deleteBinNegative
       When we perform a DELETE against context(url)/b/<BIN_ID>  using payload of context(payload) using deleteBinNegative
       Then assert that 400 Unauthorized will be equal to object(deleteBinNegative.statuscode) as expected
       And assert that object(deleteBinNegative.message) will be equal to Invalid Bin Id provided as expected


       Scenario: User deletes a Bin <param>
        Given we prepare a new api session called deleteBinNegative
        When we perform a DELETE against context(url)b/<BIN_ID>  using payload of context(payload) using deleteBinNegative
        Then assert that 401 unauthorized will be equal to object(deleteBinNegative.statuscode) as expected
        And assert that object(deleteBinNegative.message) will be equal to <message> as expected
        Examples:
            | param                                             | X-Master-Key                       | message
            | Empty X-Master-Key                                |                                    | You need to pass X-Master-Key in the header        |
            | Invalid X-Master-Key                              | random invalid value               | Invalid X-Master-Key provided                      |
            | Invalid Bin                                       | Random Bin_id                      | Bin not found or it doesn't belong to your account |


       Scenario: User deletes a Bin <param>
        Given we prepare a new api session called deleteBinNegative
        When we perform a DELETE against context(url)b/<BIN_ID>  using payload of context(payload) using deleteBinNegative
        Then assert that 403 Forbidden will be equal to object(deleteBinNegative.statuscode) as expected
        And assert that object(deleteBinNegative.message) will be equal to Requests exhausted. Buy additional requests at https://jsonbin.io/pricing as expected








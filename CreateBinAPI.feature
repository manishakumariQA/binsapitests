Feature: Bins API - Create

Background: Init Step
            Given we prepare a new api session called createBinAPITest
            Given we store {"url":"https://api.jsonbin.io/v3"} in the context


  Scenario: User creates a Private Bin
    Given we prepare a new api session called createPrivateBin
    And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to createPrivateBin
    And we add a {"Content-Type": "application/json"} headers to createPrivateBin
    And we add a {"X-Bin-Name": "BIN_NAME"} headers to createPrivateBin
    When we perform a POST against context(url) using payload of none using createPrivateBin
    Then assert that 200 will be equal to object(createPrivateBin.statuscode) as expected
    And response includes the following
    	| record 	 		| 1 		  |
    	| sample			| Hello World |
    	| id			    | BIN_ID	  |
    	| createdAt			| DATE/TIME	  |
    	| private			| true		  |

    Scenario: User creates a Public Bin
     Given we prepare a new api session called createPublicBin
     And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to createPublicBin
     And we add a {"Content-Type": "application/json"} headers to createPublicBin
     And we add a {"X-Bin-Name": "BIN_NAME"} headers to createPublicBin
     And we add a {"X-Bin-Private": "false"} headers to createPublicBin
     When we perform a POST against context(url) using payload of none using createPublicBin
     Then assert that 200 will be equal to object(createPublicBin.statuscode) as expected
     And response includes the following
         | record 	 		| 1 					|
         | sample			| Hello World			|
         | id			    | BIN_ID			    |
         | createdAt		| DATE/TIME			    |
         | private			| false			        |

     Scenario: User creates a Bin inside a collection
      Given we prepare a new api session called createBinInsideCollection
      And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to createBinInsideCollection
      And we add a {"Content-Type": "application/json"} headers to createBinInsideCollection
      And we add a {"X-Bin-Name": "BIN_NAME"} headers to createBinInsideCollection
      And we add a {"X-Collection-Id": "COLLECTION_ID"} headers to createBinInsideCollection
      When we perform a POST against context(url) using payload of none using createPublicBin
      Then assert that 200 will be equal to object(createPublicBin.statuscode) as expected
      And response includes the following
          | record 	 		| 1 					|
          | sample			| Hello World			|
          | id			    | BIN_ID			    |
          | createdAt		| DATE/TIME			    |
          | private			| true			        |

      Scenario: User creates a Bin <param>
       Given we prepare a new api session called createBinNegative
       And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to createBinInsideCollection
       When we perform a POST against context(url) using payload of none using createBinNegative
       Then assert that 400 Bad Request will be equal to object(createBinNegative.statuscode) as expected
       And assert that object(createBinNegative.message) will be equal to <message> as expected
       Examples:
           | param                                             | Headers                              | message                                                                             |
           | Content-Type is Empty                             | Content-Type =""                     | You need to pass Content-Type set to application/json                               |
           | Bin name is blank                                 | X-Bin-Name =""                       | Bin cannot be blank                                                                 |
           | X-Bin-Name cannot be blank or over 128 characters | X-Bin-Name =more than 128 characters |             | X-Bin-Name cannot be blank or over 128 characters                     |
           | Invalid X-Collection-Id provided                  | X-Collection-Id =blabla              | Invalid X-Collection-Id provided                                                    |
           | Collection not found                              | X-Collection-Id = 6575969            | Collection not found or the Collection does not belong to the X-Master-Key provided |
           | Schema Doc Validation Mismatch: key:val           | Key =yhhhkih68                       | Schema Doc Validation Mismatch: key:val                                             |



      Scenario: User creates a Bin <param>
        Given we prepare a new api session called createBinNegative
        And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to createBinInsideCollection
        When we perform a POST against context(url) using payload of none using createBinNegative
        Then assert that 403 Forbidden will be equal to object(createBinNegative.statuscode) as expected
        And assert that object(createBinNegative.message) will be equal to <message> as expected
        Examples:
            | param                                             | Headers                              | message                                                                                                                 |
            | Free User                                         | Free User                            | Free users cannot create a record over 500kb. Upgrade to Pro plan https://jsonbin.io/pricing to create records upto 1mb |
            | Request Exhausted                                 | Limited Requests                     | Requests exhausted. Buy additional requests at https://jsonbin.io/pricing                                               |


       Scenario: User creates a Bin without X-Master-Key
        Given we prepare a new api session called createBinNegative
        When we perform a POST against context(url) using payload of none using createBinNegative
        Then assert that 401 unauthorized will be equal to object(createBinNegative.statuscode) as expected
        And assert that object(createBinNegative.message) will be equal to You need to pass X-Master-Key in the header as expected


       Scenario: User searches for an invalid Bin
        Given we prepare a new api session called createBinNegative
        When we perform a POST against context(url) using payload of none using createBinNegative
        Then assert that 404 Not Found will be equal to object(createBinNegative.statuscode) as expected
        And assert that object(createBinNegative.message) will be equal to Bin not found or it doesn't belong to your account as expected




	Given we store a  book exists with an isbn of 9781451648546
	When a user retrieves the book by isbn
	Then the status code is 200
	And response includes the following
	| totalItems 	 		| 1 					|
	| kind					| books#volumes			|
   And response includes the following in any order
	| items.volumeInfo.title 					| Steve Jobs			|
	| items.volumeInfo.publisher 				| Simon and Schuster	|
	| items.volumeInfo.pageCount 				| 630					|








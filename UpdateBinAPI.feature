Feature: Bins API - Update

Background: Init Step
            Given we prepare a new api session called updateBinAPITest
            Given we store {"url":"https://api.jsonbin.io/v3"} in the context


  Scenario: User updates a Private Bin with versioning off
    Given we prepare a new api session called updateBin
    And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
    And we add a {"Content-Type": "application/json"} headers to updateBin
    And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to updateBin
    When we perform a PUT against context(url)/b/<BIN_ID> using payload of context(payload) using updateBin
    Then assert that 200 will be equal to object(updateBin.statuscode) as expected
    And response includes the following
    	| record 	 		| 1 		  |
    	| sample			| Hello World |
    	| parentId			| BIN_ID	  |
    	| private			| true		  |

  Scenario: User updates a Private Bin with versioning on
      Given we prepare a new api session called updateBin
      And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
      And we add a {"Content-Type": "application/json"} headers to updateBin
      And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to updateBin
      And we add a {"X-Bin-Versioning":"true"} headers to updateBin
      When we perform a PUT against context(url)/b/<BIN_ID> using payload of context(payload) using updateBin
      Then assert that 200 will be equal to object(updateBin.statuscode) as expected
      And response includes the following
      	| record 	 		| 1 		  |
      	| sample			| Hello World |
      	| parentId			| BIN_ID	  |
      	| private			| true		  |
      	| versioning        | true        |

  Scenario: User updates a Public Bin with versioning on
      Given we prepare a new api session called updateBin
      And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
      And we add a {"Content-Type": "application/json"} headers to updateBin
      When we perform a PUT against context(url)/b/<BIN_ID> using payload of context(payload) using updateBin
      Then assert that 200 will be equal to object(updateBin.statuscode) as expected
      And response includes the following
        | record 	 		| 1 		  |
        | sample			| Hello World |
        | parentId			| BIN_ID	  |
        | private			| false		  |

  Scenario: User updates a Public Bin with versioning off
              Given we prepare a new api session called updateBin
              And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
              And we add a {"Content-Type": "application/json"} headers to updateBin
              And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to updateBin
              And we add a {"X-Bin-Versioning":"false"} headers to updateBin
              When we perform a PUT against context(url)/b/<BIN_ID> using payload of context(payload) using updateBin
              Then assert that 200 will be equal to object(updateBin.statuscode) as expected
              And response includes the following
              	| record 	 		| 1 		  |
              	| sample			| Hello World |
              	| parentId			| BIN_ID	  |
              	| private			| true		  |
              	| versioning        | false        |


      Scenario: User updates a Bin <param>
       Given we prepare a new api session called updateBinNegative
       And we store {"payload": {"BIN_ID": "<BIN_ID>"} } in the context
       And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to updateBinNegative
       When we perform a PUT against context(url)/b/<BIN_ID>  using payload of none using updateBinNegative
       Then assert that 400 Bad Request will be equal to object(updateBinNegative.statuscode) as expected
       And assert that object(updateBinNegative.message) will be equal to <message> as expected
       Examples:
           | param                                             | Headers                              | message                                               |
           | No Content-Type                                   | Invalid Content-Type                 | You need to pass Content-Type set to application/json |
           | Bin_Id is invalid                                 | Invalid BIN_Id                       | Invalid Bin Id provided                               |
           | Blank Bin                                         | Blank Bin                            | Bin cannot be blank                                   |
           | Schema Doc Validation Mismatch: key:val           | Bad schema                           | Schema Doc Validation Mismatch: key:val               |


      Scenario: User updates a Bin <param>
        Given we prepare a new api session called updateBinNegative
        And we store {"payload": {"BIN_ID": "<Invalid_BIN_ID>"} } in the context
        And we add a {"X-Master-Key":"<YOUR_API_KEY>"} headers to updateBinNegative
        When we perform a PUT against context(url)b/<BIN_ID>  using payload of context(payload) using updateBinNegative
        Then assert that 404 Not Found will be equal to object(updateBinNegative.statuscode) as expected
        And assert that object(updateBinNegative.message) will be equal to Bin not found as expected


       Scenario: User updates a Bin <param>
        Given we prepare a new api session called updateBinNegative
        When we perform a POST against context(url)b/<BIN_ID>  using payload of none using updateBinNegative
        Then assert that 401 unauthorized will be equal to object(updateBinNegative.statuscode) as expected
        And assert that object(updateBinNegative.message) will be equal to <message> as expected
        Examples:
            | param                                             | X-Master-Key                       | message
            | Empty X-Master-Key                                |                                    | You need to pass X-Master-Key in the header to update a private bin      |
            | Invalid X-Master-Key                              | random invalid value               | Invalid X-Master-Key provided or the bin does not belong to your account |


       Scenario: User updates a Bin <param>
        Given we prepare a new api session called updateBinNegative
        When we perform a POST against context(url)b/<BIN_ID>  using payload of none using updateBinNegative
        Then assert that 403 Forbidden will be equal to object(updateBinNegative.statuscode) as expected
        And assert that object(updateBinNegative.message) will be equal to <message> as expected
        Examples:
            | param                                             | Header               | message
            | Free users                                        |    Free User         | Free users cannot update a record over 500kb. Upgrade to Pro plan https://jsonbin.io/pricing to update records upto 1mb |
            | Too many Requests                                 | random requests      | Requests exhausted. Buy additional requests at https://jsonbin.io/pricing                                               |
            | Versioning                                        | Ivalid versioning    | Versioning is not available for the Free users. Upgrade to Pro plan https://jsonbin.io/pricing to avail this feature    |








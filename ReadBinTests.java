package services;

import org.apache.http.HttpStatus;
import org.junit.Test;

import java.util.Arrays;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;


public class ReadBinTests
{
	private static String readBin = "https://api.jsonbin.io/v3";

	@Test
	public void readBin(){
		String key = "X-Master-Key":"<YOUR_API_KEY>";

		given().
		param("key", "X-Master-Key").
		when().
		get(readBin)
		.then().
		statusCode(HttpStatus.SC_OK).
		body(	"BIN_ID", equalTo(1),
				"X-Master-Key", equalTo("X-Master-Key");
}
